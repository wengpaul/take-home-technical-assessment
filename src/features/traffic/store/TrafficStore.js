import { makeAutoObservable, runInAction } from "mobx";
import React from "react";
import { getTrafficImagesList, getWeatherForecast } from "../../../services/dataService";
import { geocodeLatLng, getDistanceFromLatLonInKm } from "../../../util/GeoUtil";


export class TrafficStore {
    trafficList = [];
    selectedTrafficIndex = -1;
    isLoading = false;
    isErrorLoadingData = false;

    constructor() {
        makeAutoObservable(this);
    }

    setSelectedTrafficIndex (index) {
        this.selectedTrafficIndex = index;
    }

    getTrafficData (dateTime) {
        // Set is loading to display skeleton on UI
        this.isLoading = true;
        this.isErrorLoadingData = false;

        // method to process the incoming data.
        const populateTrafficImageWithLocationName = async (trafficImageList, weatherResult) => {
            return new Promise((resolve, reject) => {
                // Find closest weather area data tied to traffic image.
                Promise.all(trafficImageList.map(async trafficImage => {
                    let closestAreaIndex = 0;
                    let closestAreaDistance = Number.MAX_SAFE_INTEGER;

                    weatherResult.area_metadata.forEach((area, index) => {
                        let areaLat = area.label_location.latitude;
                        let areaLon = area.label_location.longitude;

                        let trafficLat = trafficImage.location.latitude;
                        let trafficLon = trafficImage.location.longitude;

                        let distance = getDistanceFromLatLonInKm(areaLat, areaLon, trafficLat, trafficLon);
                        if (distance < closestAreaDistance) {
                            closestAreaIndex = index;
                            closestAreaDistance = distance;
                        }
                    });

                    let tempAreaName = await geocodeLatLng(trafficImage.location.latitude, trafficImage.location.longitude)
                    // If there is some issues with google geocoding. Eg. no API key, fall back to weather API
                    if (tempAreaName === "") {
                        tempAreaName = weatherResult.area_metadata[closestAreaIndex].name
                    }

                    // Set the closes area name found and the forecast for that area in the element
                    return {
                        ...trafficImage,
                        areaName: tempAreaName,
                        forecast: (weatherResult.items[0].forecasts.find(area => area.area === weatherResult.area_metadata[closestAreaIndex].name).forecast)
                    };
                })).then(finalTrafficImageList => {
                    // Change the state in store using runInAction for proper synchronisation
                    runInAction(() => {
                        this.trafficList = finalTrafficImageList;

                        // If there is some data we initialise the selected item in the list to first item.
                        if (finalTrafficImageList.length > 0) {
                            this.selectedTrafficIndex = 0;
                        } else {
                            this.selectedTrafficIndex = -1;
                            this.isErrorLoadingData = true;
                        }
                        this.isLoading = false;
                        resolve()
                    });
                }).catch(err => {
                    reject(err)
                })
            })
        }

        getTrafficImagesList(dateTime).then((result) => {
            return result;
        }).then(trafficImageList => {
            getWeatherForecast(dateTime).then(area_metadata => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await populateTrafficImageWithLocationName(trafficImageList, area_metadata)
                        resolve();
                    } catch (err) {
                        reject(err)
                    }
                });
            }).catch(err => {
                runInAction(() => {
                    this.isErrorLoadingData = true;
                    this.isLoading = false;
                });
            })
        }).catch(err => {
            runInAction(() => {
                this.isErrorLoadingData = true;
                this.isLoading = false;
            });
        })
    }

}

export const TrafficStoreContext = React.createContext(new TrafficStore());
