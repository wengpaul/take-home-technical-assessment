import { when } from "mobx";
import * as CONSTANTS from "../../../config/index";
import * as DataService from "../../../services/dataService";
import * as GeoUtil from "../../../util/GeoUtil";
import { TrafficStore } from "./TrafficStore";

test('Gets empty traffic data', () => {

    const trafficStore = new TrafficStore();

    const spy = jest.spyOn(DataService, 'getTrafficImagesList');
    const spy2 = jest.spyOn(DataService, 'getWeatherForecast');
    spy.mockReturnValue(new Promise((resolve, reject) => { resolve([]) }));
    spy2.mockReturnValue(new Promise((resolve, reject) => { resolve([]) }));

    trafficStore.getTrafficData("");
    expect(trafficStore.trafficList).toEqual([]);  // Success!
});

test('Gets valid traffic data', (done) => {

    const trafficStore = new TrafficStore();

    const trafficData = [
        {
            "timestamp": "2021-10-21T17:26:23+08:00",
            "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
            "location": {
                "latitude": 1.29531332,
                "longitude": 103.871146
            },
            "camera_id": "1001",
            "image_metadata": {
                "height": 240,
                "width": 320,
                "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
            }
        }]

    const weatherData = {
        "area_metadata": [
            {
                "name": "Ang Mo Kio",
                "label_location": {
                    "latitude": 1.29531332,
                    "longitude": 103.871146
                }
            },
            {
                "name": "Bedok",
                "label_location": {
                    "latitude": 1.321,
                    "longitude": 103.924
                }
            }],
        "items": [
            {
                "update_timestamp": "2021-10-21T17:08:52+08:00",
                "timestamp": "2021-10-21T17:00:00+08:00",
                "valid_period": {
                    "start": "2021-10-21T17:00:00+08:00",
                    "end": "2021-10-21T19:00:00+08:00"
                },
                "forecasts": [
                    {
                        "area": "Ang Mo Kio",
                        "forecast": "Partly Cloudy (Day)"
                    },
                    {
                        "area": "Bedok",
                        "forecast": "Partly Cloudy (Day)"
                    }]
            }
        ],
        "api_info": {
            "status": "healthy"
        }
    }

    const expected = [{
        "timestamp": "2021-10-21T17:26:23+08:00",
        "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
        "location": {
            "latitude": 1.29531332,
            "longitude": 103.871146
        },
        "camera_id": "1001",
        "image_metadata": {
            "height": 240,
            "width": 320,
            "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
        },
        areaName: "Ang Mo Kio",
        forecast: "Partly Cloudy (Day)"
    }]

    const spy = jest.spyOn(DataService, 'getTrafficImagesList');
    const spy2 = jest.spyOn(DataService, 'getWeatherForecast');
    const spy3 = jest.spyOn(GeoUtil, 'geocodeLatLng');

    spy.mockReturnValue(new Promise((resolve, reject) => { resolve(trafficData) }));
    spy2.mockReturnValue(new Promise((resolve, reject) => { resolve(weatherData) }));
    spy3.mockReturnValue(new Promise((resolve, reject) => { resolve("") }));

    trafficStore.getTrafficData("");
    when(() => trafficStore.trafficList.length > 0, () => {
        expect(trafficStore.trafficList).toEqual(expected);  // Success!
        done();
    })
});

test('Gets invalid traffic data', (done) => {

    const trafficStore = new TrafficStore();

    const trafficData = [
        {
            "timestamp": "2021-10-21T17:26:23+08:00",
            "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
            "location": {
                "latitude": 1.29531332,
                "longitude": 103.871146
            },
            "camera_id": "1001",
            "image_metadata": {
                "height": 240,
                "width": 320,
                "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
            }
        }]

    const weatherData = {
        "area_metadata": [],
        "items": [],
        "api_info": {
            "status": "healthy"
        }
    }

    const expected = [{
        "timestamp": "2021-10-21T17:26:23+08:00",
        "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
        "location": {
            "latitude": 1.29531332,
            "longitude": 103.871146
        },
        "camera_id": "1001",
        "image_metadata": {
            "height": 240,
            "width": 320,
            "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
        },
        areaName: "Ang Mo Kio",
        forecast: "Partly Cloudy (Day)"
    }]

    const spy = jest.spyOn(DataService, 'getTrafficImagesList');
    const spy2 = jest.spyOn(DataService, 'getWeatherForecast');
    spy.mockReturnValue(new Promise((resolve, reject) => { resolve(trafficData) }));
    spy2.mockReturnValue(new Promise((resolve, reject) => { resolve(weatherData) }));
    trafficStore.getTrafficData("");
    when(() => trafficStore.isErrorLoadingData, () => {
        expect(trafficStore.isErrorLoadingData).toEqual(true);  // Success!
        done();
    })
});



test('getTrafficImagesList throws', (done) => {

    const trafficStore = new TrafficStore();

    const trafficData = [
        {
            "timestamp": "2021-10-21T17:26:23+08:00",
            "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
            "location": {
                "latitude": 1.29531332,
                "longitude": 103.871146
            },
            "camera_id": "1001",
            "image_metadata": {
                "height": 240,
                "width": 320,
                "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
            }
        }]

    const weatherData = {
        "area_metadata": [],
        "items": [],
        "api_info": {
            "status": "healthy"
        }
    }

    const expected = [{
        "timestamp": "2021-10-21T17:26:23+08:00",
        "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
        "location": {
            "latitude": 1.29531332,
            "longitude": 103.871146
        },
        "camera_id": "1001",
        "image_metadata": {
            "height": 240,
            "width": 320,
            "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
        },
        areaName: "Ang Mo Kio",
        forecast: "Partly Cloudy (Day)"
    }]

    const spy = jest.spyOn(DataService, 'getTrafficImagesList');
    const spy2 = jest.spyOn(DataService, 'getWeatherForecast');
    spy.mockReturnValue(new Promise((resolve, reject) => { reject("err") }));
    spy2.mockReturnValue(new Promise((resolve, reject) => { resolve(weatherData) }));
    trafficStore.getTrafficData("");
    when(() => trafficStore.isErrorLoadingData, () => {
        expect(trafficStore.isErrorLoadingData).toEqual(true);  // Success!
        done();
    })
});
