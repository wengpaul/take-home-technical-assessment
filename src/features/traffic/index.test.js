import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import * as React from 'react';
import TrafficHomepage from '.';
import { TrafficStore, TrafficStoreContext } from '../traffic/store/TrafficStore';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateAdapter from '@mui/lab/AdapterMoment';
import moment from 'moment'
import { when } from 'mobx';

test('select a date time', async () => {
    return new Promise(async (resolve, reject) => {
        let trafficStore = new TrafficStore();
        trafficStore.isLoading = false;
        trafficStore.selectedTrafficIndex = 0;
        trafficStore.trafficList = [{ areaName: "areaname1" }, { areaName: "areaname3" }]

        render(
            <TrafficStoreContext.Provider value={trafficStore}>
                < LocalizationProvider dateAdapter={DateAdapter} >

                    <TrafficHomepage></TrafficHomepage>
                </LocalizationProvider>
            </TrafficStoreContext.Provider>
        );

        await fireEvent.click(screen.getAllByRole('button')[0]);

        // We click two days to toggle if it lands on today
        let day = await waitFor(async () => screen.getByText("21"));
        fireEvent.click(day);

        day = await waitFor(async () => screen.getByText("20"));
        fireEvent.click(day);

        when(() => trafficStore.isLoading === true, () => { resolve() })
    })
});

test('select invalid', async () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.isErrorLoadingData = true;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ areaName: "areaname1" }, { areaName: "areaname3" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            < LocalizationProvider dateAdapter={DateAdapter} >
                <TrafficHomepage></TrafficHomepage>
            </LocalizationProvider>
        </TrafficStoreContext.Provider>
    );

});
