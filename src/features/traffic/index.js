import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles';
import TimePicker from '@mui/lab/TimePicker';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import * as React from 'react';
import TextField from '@mui/material/TextField';
import moment from 'moment';
import TrafficList from './components/TrafficList';
import * as CONSTANTS from './../../config'
import { observer } from "mobx-react-lite";
import { TrafficStoreContext } from './store/TrafficStore';
import TrafficImage from './components/TrafficImage';
import AreaWeather from './components/AreaWeather';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '1% 5% 5% 5%',
        minWidth: "250px"
    },
    card: {
        margin: '10px 10px 10px 10px'
    },
    pickers: {
        textAlign: 'left',
        width: '100%'
    },
    header: {
        textAlign: 'left',
        padding: '0px 0px 20px 0px'
    },
    warningMessage: {
        textAlign: 'left',
        padding: "10px 10px 10px 10px"
    }
}));

const TrafficHomepage = observer(() => {
    const classes = useStyles();
    const [value, setValue] = React.useState(moment());
    const trafficStore = React.useContext(TrafficStoreContext);

    const handleChange = (newValue) => {
        trafficStore.getTrafficData(newValue.format(CONSTANTS.DATE_FORMAT));
        setValue(newValue);
    };

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12} className={classes.header}>
                <Typography variant="h4" sx={{ color: 'primary.main' }}>Traffic WebApp</Typography>
                <Typography variant="subtitle1" color="text.secondary">Search traffic condition and weather forecast</Typography>
            </Grid>

            <Grid container item xs={12} wrap='nowrap'>
                <Grid item >
                    <DesktopDatePicker
                        className={classes.pickers}
                        label="Date"
                        inputFormat="MM/DD/YYYY"
                        value={value}
                        onChange={handleChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                </Grid>
                <Grid item >
                    <TimePicker
                        className={classes.pickers}
                        label="Time"
                        value={value}
                        onChange={handleChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                </Grid>
            </Grid>
            {
                trafficStore.trafficList.length === 0 && !trafficStore.isErrorLoadingData && !trafficStore.isLoading ?
                    <Grid item xs={12} className={classes.warningMessage}>
                        <Typography>Please select a date and time...</Typography>
                    </Grid> :
                    trafficStore.isErrorLoadingData && !trafficStore.isLoading ?
                        <Grid item xs={12} className={classes.warningMessage}>
                            <Typography>Something went wrong. Please select a another date and time...</Typography>
                        </Grid> :
                        <>
                            <Grid item xs={12} sm={8}>
                                <TrafficList ></TrafficList>
                            </Grid>
                            <Grid item xs={12} sm={4} >
                                <AreaWeather></AreaWeather>
                            </Grid>
                            <Grid item xs={12} sm={8} >
                                <TrafficImage></TrafficImage>
                            </Grid>
                        </>
            }
        </Grid>
    );
});

export default TrafficHomepage;
