import { makeStyles } from '@mui/styles';
import * as React from 'react';
import { TrafficStoreContext } from '../store/TrafficStore';
import { observer } from "mobx-react-lite";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Skeleton from '@mui/material/Skeleton';

const useStyles = makeStyles(() => ({
    root: {
        padding: '10px 10px 10px 10px'
    },
    card: {
        margin: '10px 10px 10px 10px'
    }
}));

let AreaWeather = observer(() => {

    const classes = useStyles();
    const trafficStore = React.useContext(TrafficStoreContext);

    return (
        <Card className={classes.card}>

            {trafficStore.isLoading ?
                <>
                    <Skeleton animation="wave" width="60%" />
                    <Skeleton animation="wave" width="100%" />
                </>
                :
                <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        Weather
                    </Typography>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>

                        {
                            trafficStore.selectedTrafficIndex > -1 &&
                                trafficStore.trafficList[trafficStore.selectedTrafficIndex].forecast ? trafficStore.trafficList[trafficStore.selectedTrafficIndex].forecast : ""}
                    </Typography>
                </CardContent>
            }
        </Card>
    );
});

export default AreaWeather;
