import { render, screen } from '@testing-library/react';
import AreaWeather from '../AreaWeather';
import * as React from 'react';
import { TrafficStore, TrafficStoreContext } from '../../store/TrafficStore';

test('renders loading skeleton', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = true;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ forecast: "Good" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <AreaWeather />
        </TrafficStoreContext.Provider>);
    const linkElement = screen.queryByText(/Good/i);  // substring match, ignore case
    expect(linkElement).toBeNull();
});

test('renders weather', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ forecast: "Good" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <AreaWeather />
        </TrafficStoreContext.Provider>);
    const linkElement = screen.queryByText(/Good/i);  // substring match, ignore case
    expect(linkElement).toBeValid();
});

test('rendersnon existent weather', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = -1;
    trafficStore.trafficList = [{ forecast: "Good" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <AreaWeather />
        </TrafficStoreContext.Provider>);
    const linkElement = screen.queryByText(/Good/i);  // substring match, ignore case
    expect(linkElement).toBeNull();
});
