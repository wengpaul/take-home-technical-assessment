import { render, screen } from '@testing-library/react';
import AreaWeather from '../AreaWeather';
import * as React from 'react';
import { TrafficStore, TrafficStoreContext } from '../../store/TrafficStore';
import TrafficImage from '../TrafficImage';

test('renders loading skeleton', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = true;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ forecast: "Good" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <TrafficImage />
        </TrafficStoreContext.Provider>);

    const image = screen.queryByRole('img');
    expect(image).toBeNull();
});

test('renders image', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ image: "Good" }]
    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <TrafficImage />
        </TrafficStoreContext.Provider>);

    const image = screen.getByRole('img');
    expect(image).toBeValid();
    expect(image.src).toContain('Good')
});

test('renders non-existent image', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = -1;
    trafficStore.trafficList = [{ image: "Good" }]
    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <TrafficImage />
        </TrafficStoreContext.Provider>);

    const image = screen.queryByRole('img');
    expect(image).toBeNull();
});
