import { fireEvent, render, screen } from '@testing-library/react';
import { when } from 'mobx';
import * as React from 'react';
import { TrafficStore, TrafficStoreContext } from '../../store/TrafficStore';
import TrafficList from '../TrafficList';
import { TrafficListItem } from '../TrafficList'

test('renders loading skeleton', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = true;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ areaName: "areaname1" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <TrafficList />
        </TrafficStoreContext.Provider>);
    const linkElement = screen.queryByText(/areaname1/i);  // substring match, ignore case
    expect(linkElement).toBeNull();
});

test('renders list', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ areaName: "areaname1" }]

    render(
        <TrafficStoreContext.Provider value={trafficStore}>
            <TrafficList />
        </TrafficStoreContext.Provider>);

    const linkElement = screen.queryByText(/areaname1/i);  // substring match, ignore case
    expect(linkElement).toBeValid();

});

test('click a row', () => {

    let trafficStore = new TrafficStore();
    trafficStore.isLoading = false;
    trafficStore.selectedTrafficIndex = 0;
    trafficStore.trafficList = [{ areaName: "areaname1" }, { areaName: "areaname3" }]

    render(
        <TrafficListItem index={1} trafficStore={trafficStore} data={trafficStore.trafficList}></TrafficListItem>
    );

    fireEvent.click(screen.getByRole('button'));

    when(() => trafficStore.selectedTrafficIndex !== 0, () => {
        expect(trafficStore.selectedTrafficIndex).toEqual(1);
    })
});
