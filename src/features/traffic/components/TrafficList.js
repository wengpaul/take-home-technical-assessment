import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles';
import * as React from 'react';
import { FixedSizeList } from 'react-window';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import { TrafficStoreContext } from '../store/TrafficStore';
import { observer } from "mobx-react-lite";
import Skeleton from '@mui/material/Skeleton';
import moment from 'moment';
import * as CONSTANTS from '../../../config';

const useStyles = makeStyles(() => ({
    root: {
        padding: '10px 0px 10px 0px'
    },
    card: {
        margin: '10px 10px 10px 10px'
    }
}));

export const TrafficListItem = ({ index, trafficStore, data }) => {
    return (
        <ListItemButton style={{ height: "100%" }}
            selected={index === trafficStore.selectedTrafficIndex}
            onClick={() => {
                trafficStore.setSelectedTrafficIndex(index);
            }}>
            <Grid container>
                <Grid item xs={12}>
                    <Typography noWrap variant="body1">{data[index].areaName}</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body2" sx={{ color: "text.secondary" }}>{moment(data[index].timestamp).format(CONSTANTS.DATE_FORMAT_DISPLAY)}</Typography>
                </Grid>
            </Grid>
        </ListItemButton>
    )
}

let TrafficList = observer(() => {

    const classes = useStyles();
    const trafficStore = React.useContext(TrafficStoreContext);
    let data = trafficStore.trafficList;

    // Observe selected traffic index for updating selected traffic image in the list.
    React.useEffect(() => {
    }, [trafficStore.selectedTrafficIndex]);

    let renderRow = (props) => {
        const { index, style } = props;
        return (
            <ListItem style={{ ...style, }} key={index} component="div" disablePadding>
                <TrafficListItem index={index} trafficStore={trafficStore} data={data} ></TrafficListItem>
            </ListItem >
        );
    }

    const renderSkeleton = () => {
        return (
            <>
                {
                    // height of skeleton to match height of traffic list.
                    Array.from({ length: 8 }, (_, i) =>
                        <Skeleton
                            key={i}
                            height={50}
                            animation="wave"
                            width="100%" />
                    )
                }
            </>
        );
    }

    return (
        <Grid container item className={classes.root}>
            {trafficStore.isLoading ?
                renderSkeleton() :
                <FixedSizeList
                    height={400}
                    width="100%"
                    itemSize={50}
                    itemCount={data.length}
                    overscanCount={5}
                >
                    {renderRow}
                </FixedSizeList>
            }

        </Grid>
    );
});

export default TrafficList;
