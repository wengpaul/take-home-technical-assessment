import { makeStyles } from '@mui/styles';
import * as React from 'react';
import { TrafficStoreContext } from '../store/TrafficStore';
import { observer } from "mobx-react-lite";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Skeleton from '@mui/material/Skeleton';

const useStyles = makeStyles(() => ({
    card: {
        margin: '10px 10px 10px 10px'
    }
}));

let TrafficImage = observer(() => {

    const classes = useStyles();
    const trafficStore = React.useContext(TrafficStoreContext);

    return (
        <Card className={classes.card}>
            {trafficStore.isLoading ?
                <Skeleton animation="wave" width="100%" height={"400px"} /> :
                <CardContent>
                    {trafficStore.selectedTrafficIndex > -1 ?
                        <img
                            src={trafficStore.trafficList[trafficStore.selectedTrafficIndex].image}
                            alt="Logo"
                            width="100%"
                            height="100%"
                            style={{ maxWidth: "400px" }}
                        /> : ""}
                </CardContent>
            }
        </Card>
    );
});

export default TrafficImage;
