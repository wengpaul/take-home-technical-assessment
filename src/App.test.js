import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Traffic WebApp/i);  // substring match, ignore case
  expect(linkElement).toBeInTheDocument();
});
