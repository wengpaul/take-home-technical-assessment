export const BACKEND_API = "https://api.data.gov.sg/v1";
export const BACKEND_API_GET_TRAFFIC_IMAGES = BACKEND_API + "/transport/traffic-images";
export const BACKEND_API_GET_WEATHER_FORECAST = BACKEND_API + "/environment/2-hour-weather-forecast";

// API date format
export const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss';

// Human readable date format for display
export const DATE_FORMAT_DISPLAY = 'YYYY-MM-DD HH:mm:ss';

// Google api key (Take from environment variables)
export const GOOGLE_API_KEY = process.env.REACT_APP_GOOGLE_API_KEY || ""