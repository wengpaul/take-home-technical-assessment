describe('environmental variables', () => {
    const OLD_ENV = process.env;

    beforeEach(() => {
        jest.resetModules() // Most important - it clears the cache
        process.env = { ...OLD_ENV }; // Make a copy
    });

    afterAll(() => {
        process.env = OLD_ENV; // Restore old environment
    });

    test('will receive process.env variables', () => {
        // Set the variables
        process.env.REACT_APP_GOOGLE_API_KEY = 'dev';
        expect(require('../config/index').GOOGLE_API_KEY).toEqual('dev')


    });

    test('google env not found', () => {
        // Set the variables
        delete process.env.REACT_APP_GOOGLE_API_KEY;
        expect(require('../config/index').GOOGLE_API_KEY).toEqual('')


    });
});