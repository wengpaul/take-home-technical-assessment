import './App.css';
import TrafficHomepage from './features/traffic';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateAdapter from '@mui/lab/AdapterMoment';

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
    secondary: {
      main: green[500],
    },
  },
});

function App () {
  return (
    // Global theme for app
    <ThemeProvider theme={theme} >
      {/* Localization provider used for date time picker */}
      < LocalizationProvider dateAdapter={DateAdapter} >
        <div className="App">
          <TrafficHomepage></TrafficHomepage>
        </div>
      </LocalizationProvider >
    </ThemeProvider >
  );
}

export default App;
