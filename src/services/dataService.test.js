import axios from 'axios';
import { getTrafficImagesList, getWeatherForecast } from './dataService';

jest.mock('axios');

test('should get TrafficImagesList', () => {

    const response = {
        data: {
            "items": [
                {
                    "timestamp": "2021-10-21T17:26:43+08:00",
                    "cameras": [
                        {
                            "timestamp": "2021-10-21T17:26:23+08:00",
                            "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
                            "location": {
                                "latitude": 1.29531332,
                                "longitude": 103.871146
                            },
                            "camera_id": "1001",
                            "image_metadata": {
                                "height": 240,
                                "width": 320,
                                "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
                            }
                        }]
                }
            ],
            "api_info": {
                "status": "healthy"
            }
        }
    }

    const expected = [
        {
            "timestamp": "2021-10-21T17:26:23+08:00",
            "image": "https://images.data.gov.sg/api/traffic-images/2021/10/e82fe65a-6e36-41af-97e0-cffb4562b46b.jpg",
            "location": {
                "latitude": 1.29531332,
                "longitude": 103.871146
            },
            "camera_id": "1001",
            "image_metadata": {
                "height": 240,
                "width": 320,
                "md5": "8b1e07ade4e52fe26b75e5297aa33a01"
            }
        }]

    axios.get.mockResolvedValue(response);

    // or you could use the following depending on your use case:
    // axios.get.mockImplementation(() => Promise.resolve(resp))

    return getTrafficImagesList().then(data => expect(data).toEqual(expected));
});


test('should not get TrafficImagesList', () => {

    axios.get.mockRejectedValueOnce("err");

    // or you could use the following depending on your use case:
    // axios.get.mockImplementation(() => Promise.resolve(resp))

    // eslint-disable-next-line jest/no-conditional-expect
    return getTrafficImagesList().catch(data => expect(data).toEqual("err"));
});

test('should not get TrafficImagesList (Empty)', () => {

    const response = {
        data: {
            "items": [
                {
                    "timestamp": "2021-10-21T17:26:43+08:00"
                }
            ],
            "api_info": {
                "status": "healthy"
            }
        }
    }

    axios.get.mockResolvedValue(response);

    // or you could use the following depending on your use case:
    // axios.get.mockImplementation(() => Promise.resolve(resp))

    // eslint-disable-next-line jest/no-conditional-expect
    return getTrafficImagesList().catch(data => expect(data).toEqual([]));
});

test('should get WeatherForecast', () => {

    const response = {
        data: {
            "area_metadata": [
                {
                    "name": "Ang Mo Kio",
                    "label_location": {
                        "latitude": 1.375,
                        "longitude": 103.839
                    }
                },
                {
                    "name": "Bedok",
                    "label_location": {
                        "latitude": 1.321,
                        "longitude": 103.924
                    }
                }],
            "items": [
                {
                    "update_timestamp": "2021-10-21T17:08:52+08:00",
                    "timestamp": "2021-10-21T17:00:00+08:00",
                    "valid_period": {
                        "start": "2021-10-21T17:00:00+08:00",
                        "end": "2021-10-21T19:00:00+08:00"
                    },
                    "forecasts": [
                        {
                            "area": "Ang Mo Kio",
                            "forecast": "Partly Cloudy (Day)"
                        },
                        {
                            "area": "Bedok",
                            "forecast": "Partly Cloudy (Day)"
                        }]
                }
            ],
            "api_info": {
                "status": "healthy"
            }
        }
    }

    const expected = {
        "area_metadata": [
            {
                "name": "Ang Mo Kio",
                "label_location": {
                    "latitude": 1.375,
                    "longitude": 103.839
                }
            },
            {
                "name": "Bedok",
                "label_location": {
                    "latitude": 1.321,
                    "longitude": 103.924
                }
            }],
        "items": [
            {
                "update_timestamp": "2021-10-21T17:08:52+08:00",
                "timestamp": "2021-10-21T17:00:00+08:00",
                "valid_period": {
                    "start": "2021-10-21T17:00:00+08:00",
                    "end": "2021-10-21T19:00:00+08:00"
                },
                "forecasts": [
                    {
                        "area": "Ang Mo Kio",
                        "forecast": "Partly Cloudy (Day)"
                    },
                    {
                        "area": "Bedok",
                        "forecast": "Partly Cloudy (Day)"
                    }]
            }
        ],
        "api_info": {
            "status": "healthy"
        }
    }

    axios.get.mockResolvedValue(response);

    // or you could use the following depending on your use case:
    // axios.get.mockImplementation(() => Promise.resolve(resp))

    return getWeatherForecast().then(data => expect(data).toEqual(expected));
});


test('should not get WeatherForecast', () => {

    axios.get.mockRejectedValueOnce("err");

    // or you could use the following depending on your use case:
    // axios.get.mockImplementation(() => Promise.resolve(resp))

    // eslint-disable-next-line jest/no-conditional-expect
    return getWeatherForecast().catch(data => expect(data).toEqual("err"));
});