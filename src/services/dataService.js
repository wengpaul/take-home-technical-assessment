import axios from 'axios';
import * as CONSTANTS from './../config'

// Returns a list of traffic images
export const getTrafficImagesList = (dateTime) => {
    return new Promise((resolve, reject) => {
        axios.get(CONSTANTS.BACKEND_API_GET_TRAFFIC_IMAGES +
            '?date_time=' +
            encodeURIComponent(dateTime) //YYYY-MM-DD[T]HH:mm:ss (SGT)
        ).then(res => {
            // No cameras found. Date provided might be in the future
            res.data.items[0].cameras !== undefined ? resolve(res.data.items[0].cameras) : resolve([])
        }).catch(err => {
            reject(err)
        })
    })
}

// Returns a list of weather forecast
export const getWeatherForecast = (dateTime) => {
    return new Promise((resolve, reject) => {
        axios.get(CONSTANTS.BACKEND_API_GET_WEATHER_FORECAST +
            '?date_time=' +
            encodeURIComponent(dateTime) //YYYY-MM-DD[T]HH:mm:ss (SGT)
        ).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}