import axios from 'axios';
import * as CONSTANTS from './../config'
import { GEOCODE_MAPPING_CACHE } from './GeoUtilStore';

// https://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates
export function getDistanceFromLatLonInKm (lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad (deg) {
    return deg * (Math.PI / 180)
}

// Converts lat lon to a human readable address. Returns a promise with a string result
export function geocodeLatLng (lat, lon) {

    // Check the cache before calling google api
    const found = GEOCODE_MAPPING_CACHE.find(geocode => geocode.lat === lat && geocode.lon === lon);

    return new Promise((resolve, reject) => {
        if (found === undefined) {
            console.log("Calling google api. This may cost money!")
            console.log("Searching: ", lat, lon)
            axios.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=" + CONSTANTS.GOOGLE_API_KEY)
                .then(res => {
                    let results = res.data.results;
                    let addressName = "";

                    if (results.length > 0) {
                        // Since google return multiple address, we look for the routes first
                        results.forEach(result => {
                            if (result.types.includes("route")) {
                                addressName = result.formatted_address;
                            }
                        });

                        // If route does not exist then just take the first address
                        if (addressName === "") {
                            addressName = results[0].formatted_address;
                        }
                    }

                    // Add it to the cache
                    GEOCODE_MAPPING_CACHE.push({ lat: lat, lon: lon, addressName: addressName })
                    resolve(addressName);
                }).catch(err => {
                    reject(err)
                })

        } else {
            resolve(found.addressName)
        }
    })
}