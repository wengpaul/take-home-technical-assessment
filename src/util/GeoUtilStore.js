import { GEOCODE_MAPPING } from "../config/geocode_mapping_extracted";

// Load the const cache into a "live" cache for populating with new data points
// This CACHE is in a standalone file to allow for mocking during testing
export let GEOCODE_MAPPING_CACHE = GEOCODE_MAPPING;
