import { when } from "mobx";
import axios from 'axios';
import * as GeoUtil from "./GeoUtil";
import * as GeoUtilStore from "./GeoUtilStore"

jest.mock('axios');

test('Get geocodeLatLng from cache', (done) => {

    let expected = "Defu Flyover, Singapore2"
    GeoUtilStore.GEOCODE_MAPPING_CACHE = [{ "lat": 1.363519886, "lon": 103.905394, "addressName": expected }]
    GeoUtil.geocodeLatLng(1.363519886, 103.905394).then(result => {
        expect(result).toEqual(expected);  // Success!
        done()
    })

});

test('Get geocodeLatLng from api', (done) => {
    const response = {
        data: {
            "results":
                [{
                    "address_components":
                        [{ "long_name": "Defu Flyover", "short_name": "Defu Flyover", "types": ["route"] },
                        { "long_name": "Hougang", "short_name": "Hougang", "types": ["neighborhood", "political"] },
                        { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] },
                        { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }],
                    "formatted_address": "Defu Flyover, Singapore2",
                    "geometry": {
                        "bounds": {
                            "northeast": { "lat": 1.3636824, "lng": 103.9054274 },
                            "southwest": { "lat": 1.3635214, "lng": 103.9053386 }
                        },
                        "location": { "lat": 1.3636019, "lng": 103.905383 },
                        "location_type": "GEOMETRIC_CENTER", "viewport": {
                            "northeast": { "lat": 1.364950880291502, "lng": 103.9067319802915 },
                            "southwest": { "lat": 1.362252919708498, "lng": 103.9040340197085 }
                        }
                    },
                    "place_id": "ChIJy-nT2CwW2jERcGDtI1xMAqU",
                    "types": ["route"]
                }, { "address_components": [{ "long_name": "9W74+C5", "short_name": "9W74+C5", "types": ["plus_code"] }, { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "9W74+C5 Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.363625, "lng": 103.9055 }, "southwest": { "lat": 1.3635, "lng": 103.905375 } }, "location": { "lat": 1.3635199, "lng": 103.905394 }, "location_type": "ROOFTOP", "viewport": { "northeast": { "lat": 1.364911480291502, "lng": 103.9067864802915 }, "southwest": { "lat": 1.362213519708498, "lng": 103.9040885197085 } } }, "place_id": "GhIJgB8fPvrQ9T8RqP-s-fH5WUA", "plus_code": { "compound_code": "9W74+C5 Singapore", "global_code": "6PH59W74+C5" }, "types": ["plus_code"] }, { "address_components": [{ "long_name": "Hougang", "short_name": "Hougang", "types": ["neighborhood", "political"] }, { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "Hougang, Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.388384, "lng": 103.909961 }, "southwest": { "lat": 1.333463, "lng": 103.8732879 } }, "location": { "lat": 1.3612182, "lng": 103.8862529 }, "location_type": "APPROXIMATE", "viewport": { "northeast": { "lat": 1.388384, "lng": 103.909961 }, "southwest": { "lat": 1.333463, "lng": 103.8732879 } } }, "place_id": "ChIJWTovwj4W2jERY886y1DJqnk", "types": ["neighborhood", "political"] }, {
                    "address_components": [{ "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.4708809, "lng": 104.0415799 }, "southwest": { "lat": 1.216611, "lng": 103.6065099 } }, "location": { "lat": 1.3553794, "lng": 103.8677444 }, "location_type": "APPROXIMATE", "viewport": { "northeast": { "lat": 1.4708809, "lng": 104.0415799 }, "southwest": { "lat": 1.216611, "lng": 103.6065099 } } },
                    "place_id": "ChIJyY4rtGcX2jERIKTarqz3AAQ",
                    "types": ["locality", "political"]
                }],
            "status": "OK"
        }
    }
    GeoUtilStore.GEOCODE_MAPPING_CACHE = []
    axios.get.mockResolvedValue(response);

    GeoUtil.geocodeLatLng(1.363519886, 103.905394).then(result => {
        expect(result).toEqual("Defu Flyover, Singapore2");  // Success!
        done()
    })

});

test('Get geocodeLatLng from api (no route name)', (done) => {
    const response = {
        data: {
            "results":
                [{
                    "address_components":
                        [{ "long_name": "Defu Flyover", "short_name": "Defu Flyover", "types": ["route"] },
                        { "long_name": "Hougang", "short_name": "Hougang", "types": ["neighborhood", "political"] },
                        { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] },
                        { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }],
                    "formatted_address": "Defu Flyover, Singapore2",
                    "geometry": {
                        "bounds": {
                            "northeast": { "lat": 1.3636824, "lng": 103.9054274 },
                            "southwest": { "lat": 1.3635214, "lng": 103.9053386 }
                        },
                        "location": { "lat": 1.3636019, "lng": 103.905383 },
                        "location_type": "GEOMETRIC_CENTER", "viewport": {
                            "northeast": { "lat": 1.364950880291502, "lng": 103.9067319802915 },
                            "southwest": { "lat": 1.362252919708498, "lng": 103.9040340197085 }
                        }
                    },
                    "place_id": "ChIJy-nT2CwW2jERcGDtI1xMAqU",
                    "types": ["street"]
                }, { "address_components": [{ "long_name": "9W74+C5", "short_name": "9W74+C5", "types": ["plus_code"] }, { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "9W74+C5 Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.363625, "lng": 103.9055 }, "southwest": { "lat": 1.3635, "lng": 103.905375 } }, "location": { "lat": 1.3635199, "lng": 103.905394 }, "location_type": "ROOFTOP", "viewport": { "northeast": { "lat": 1.364911480291502, "lng": 103.9067864802915 }, "southwest": { "lat": 1.362213519708498, "lng": 103.9040885197085 } } }, "place_id": "GhIJgB8fPvrQ9T8RqP-s-fH5WUA", "plus_code": { "compound_code": "9W74+C5 Singapore", "global_code": "6PH59W74+C5" }, "types": ["plus_code"] }, { "address_components": [{ "long_name": "Hougang", "short_name": "Hougang", "types": ["neighborhood", "political"] }, { "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "Hougang, Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.388384, "lng": 103.909961 }, "southwest": { "lat": 1.333463, "lng": 103.8732879 } }, "location": { "lat": 1.3612182, "lng": 103.8862529 }, "location_type": "APPROXIMATE", "viewport": { "northeast": { "lat": 1.388384, "lng": 103.909961 }, "southwest": { "lat": 1.333463, "lng": 103.8732879 } } }, "place_id": "ChIJWTovwj4W2jERY886y1DJqnk", "types": ["neighborhood", "political"] }, {
                    "address_components": [{ "long_name": "Singapore", "short_name": "Singapore", "types": ["locality", "political"] }, { "long_name": "Singapore", "short_name": "SG", "types": ["country", "political"] }], "formatted_address": "Singapore", "geometry": { "bounds": { "northeast": { "lat": 1.4708809, "lng": 104.0415799 }, "southwest": { "lat": 1.216611, "lng": 103.6065099 } }, "location": { "lat": 1.3553794, "lng": 103.8677444 }, "location_type": "APPROXIMATE", "viewport": { "northeast": { "lat": 1.4708809, "lng": 104.0415799 }, "southwest": { "lat": 1.216611, "lng": 103.6065099 } } },
                    "place_id": "ChIJyY4rtGcX2jERIKTarqz3AAQ",
                    "types": ["locality", "political"]
                }],
            "status": "OK"
        }
    }
    GeoUtilStore.GEOCODE_MAPPING_CACHE = []
    axios.get.mockResolvedValue(response);

    GeoUtil.geocodeLatLng(1.363519886, 103.905394).then(result => {
        expect(result).toEqual("Defu Flyover, Singapore2");  // Success!
        done()
    })

});


test('Get geocodeLatLng from api (no data)', (done) => {
    const response = {
        data: {
            "results":
                [],
            "status": "OK"
        }
    }
    GeoUtilStore.GEOCODE_MAPPING_CACHE = []
    axios.get.mockResolvedValue(response);

    GeoUtil.geocodeLatLng(1.363519886, 103.905394).then(result => {
        expect(result).toEqual("");  // Success!
        done()
    })

});

test('Get geocodeLatLng from api reject', (done) => {

    GeoUtilStore.GEOCODE_MAPPING_CACHE = []
    axios.get.mockRejectedValueOnce("err");

    GeoUtil.geocodeLatLng(1.363519886, 103.905394).catch(err => {
        done()
    })

});
