# Traffic WebApp

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![](https://gitlab.com/wengpaul/take-home-technical-assessment/-/raw/main/documentation/ScreenshotMain.PNG)


## How to run the source codes
1) Check out this repository

```
git clone https://gitlab.com/wengpaul/take-home-technical-assessment
```

2) Install npm dependencies

```
cd take-home-technical-assessment
npm install
```

3) Run the app in the development mode

```
npm start
```

## How to run the test cases
This Gitlab project repository is configured to run pipelines to perform automated testing and to publish the coverage at the top of the project page. This instruction is for local testing only

Launches the test runner to collect coverage results
```
npm test
```

An example of the coverage results is as shown below. 

![](https://gitlab.com/wengpaul/take-home-technical-assessment/-/raw/main/documentation/TestResultImage.PNG)


## Notes
### Reverse Geocode
Since API 1 and API 2 have a finite set of lat lon, it is possible to cache the reverse geocode mappings to reduce api calls to the reverse geocode api. This is performed in a separate project. https://gitlab.com/wengpaul/take-home-technical-assessment-geocoder

The project basically crawl API 1 and API 2 for lat lon and perform reverse geocoding using google api and output into a file. This file is imported into this frontend project for lookup usage.

Also note that if the lookup does not contain the required lat lon datapoint, the frontend will fallback to querying google api directly.

For all google api to work, it requires a api key. See API KEY section.

### Google API Key
In order for the application to work with google api for reverse geocoding, we require an api key. This api key is secret and is not checked in to repo. 

The project searches for the google api key through the environment variables. I should have provided you a ".env" file through a different channel eg. email. If not do look for me.

If the .env is missing, The application is still usable but will fall back to using API 2 (Weather) to perform the coarse reverse geocode.

## FAQ
### Something went wrong. Please select a another date and time... 
![](https://gitlab.com/wengpaul/take-home-technical-assessment/-/raw/main/documentation/SomethingWentWrong.PNG)

If you see the above image, means that there is something wrong with the data from the api. Eg. No weather data. Even if API 1 has data, is it assumed that without API 2 data, it is still invalid. In this case please choose another date. 


